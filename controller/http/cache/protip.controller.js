const { config_protip } = require('../../../models');

class ProtipController {
  async getBySectionName(req, res) {
    try {
      const data = await config_protip.findAll({ where: { section_name: req.body.section_name } });
      res.status(200).json({
        protip: data,
      });
    } catch (error) {
      res.status(500).json({
        Success: false,
        Message: 'Data not available',
      });
    }
  }
}

module.exports = new ProtipController();
