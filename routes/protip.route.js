const express = require('express');

const router = express.Router();
const checkAuthorization = require('../controller/http/middleware/checkAuthorization');
const { JWTMiddleware } = require('../controller/http/middleware/JWT.middleware');

const ProtipController = require('../controller/http/cache/protip.controller');

router.get('/v1/getprotip', JWTMiddleware, checkAuthorization, ProtipController.getBySectionName);

module.exports = router;
